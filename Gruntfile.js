module.exports = function(grunt) {
	grunt.initConfig({
		uglify: {
			all: {
				files: {
					'./dist/glop.min.js': [ 'glop.js' ]
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.registerTask('default', ['uglify']);
};