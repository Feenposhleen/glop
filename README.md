Glop - Tiny JS events
====================

As singleton:
---------------------

	var Glop = require( 'glop' );

	Glop.setup([
		'AN_EVENT_NAME'
	]);

	Glop.on('AN_EVENT_NAME', function () {
		console.log('Never gonna give you up');
	});

	Glop.on('AN_EVENT_NAME', function () {
		console.log('Never gonna let you down');
	});

	Glop.emit('AN_EVENT_NAME');

	// console:
	// Never gonna give you up
	// Never gonna let you down

As a factory:
---------------------

	var Glop = require( 'glop' );

	var pool1 = new Glop();
	var pool2 = new Glop();

	pool1.setup([
		'NON_UNIQUE_EVENT_NAME'
	]);

	pool2.setup([
		'NON_UNIQUE_EVENT_NAME'
	]);

	pool1.on('NON_UNIQUE_EVENT_NAME', function () {
		console.log('Never gonna give you up');
	});

	pool2.on('NON_UNIQUE_EVENT_NAME', function () {
		console.log('Never gonna let you down');
	});

	pool1.emit('NON_UNIQUE_EVENT_NAME');
	pool2.emit('NON_UNIQUE_EVENT_NAME');

	// console:
	// Never gonna give you up
	// Never gonna let you down

Reference:
---------------------

**`.setup([ array eventNames ])`**  
Sets event names to be accepted in future handling. If not set, no events will be accepted. This is to limit event proliferation, and forcing a thought out selection of event names. Calling this on an already set up Glop instance will clear the instance of all registered handlers and event names, replacing them with the new ones.  
  
**`.on([ string eventName | array eventNames ], [ function eventHandler ])`**  
Attach a handler to one or more event names.  
  
**`.one([ string eventName | array eventNames ], [ function eventHandler ])`**  
Attach a handler which will only be invoked once to one or more event names. If several event names are provided, the handler will fire at most once per event name.  
  
**`.only([ string eventName | array eventNames ], [ function eventHandler ])`**  
Remove all registered handlers for one or more event names and replace it/them with a new handler.  
  
**`.onely([ string eventName | array eventNames ], [ function eventHandler ])`**  
Remove all registered handlers for one or more event names and replace it/them with a new handler function that will only fire once.  
  
**`.off([ string eventName | array eventNames ], ( function eventHandler ))`**  
De-register event handlers for one or more event names. If eventHandler is specified, only that handler function will be de-registered.  
  
**`.emit([ string eventName | array eventNames ], ( any arg1, arg2 ... ))`**  
Trigger invocation of handler functions registered to one or more event names. Any additional arguments will be passed to the invoked handler functions.  
  
**`.emitter([ string eventName ])`**  
Shorthand generator of bound emit functions. Equivalent to `.emit.bind(null, [ eventName ])`.  
  
**`.clear()`**  
Clear all registered handlers, while keeping the list of supported event names provided in `.setup([ array eventNames ])`.  
  
**`.setLogger([ function loggerFn ])`**  
Sets logger function that will receive debug information. The signature for a log function call will be `([ string message ], ( any arg1, arg2 ... ))`. The trailing arguments are debug data related to the message, for example the actual handler function registered with an `.on` call. If not set, the Glop instance will be silent.  